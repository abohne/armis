#!/usr/bin/env python

import os
import sys
from datetime import datetime
from time import sleep
from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import ASYNCHRONOUS
from influxdb_client.client.exceptions import InfluxDBError

bucket = os.getenv("DOCKER_INFLUXDB_INIT_BUCKET")
org = os.getenv("DOCKER_INFLUXDB_INIT_ORG")
token = os.getenv("DOCKER_INFLUXDB_INIT_ADMIN_TOKEN", "")

url="http://influxdb:8086"

print(f"{datetime.now()}: Setting url to {url}")
print(f"{datetime.now()}: Setting bucket to {bucket}")
print(f"{datetime.now()}: Setting org to {org}")
print(f"{datetime.now()}: Setting token to {token}")

with InfluxDBClient(url=url, bucket=bucket, org=org, token=token) as client:
    write_api = client.write_api(write_options=ASYNCHRONOUS)

    while True:
        try:
            print(f"{datetime.now()}: Collecting load averages.")
            loadavg = os.getloadavg()

            points = [
                Point("loadavg").field("1m", loadavg[0]),
                Point("loadavg").field("5m", loadavg[1]),
                Point("loadavg").field("15m", loadavg[2]),
            ]

            print(f"{datetime.now()}: Writing data to influxdb.")
            result = write_api.write(bucket=bucket, record=points)
            result.get()

            sleep(1)

        except InfluxDBError as e:
            if e.response.status == 401:
                print(f"{datetime.now()}: Insufficient write permissions to {bucket}.")

                sys.exit(1)
