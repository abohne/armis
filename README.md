# armis

- [x] Create an InfluxDB container using docker-compose. Make sure you are following best practices (or can at least explain them) in regard to mounts, network, and resources. You can, and should, use the official container from the InfluxDB docker repo.
- [x] Write a Python script to get the load average of your machine every 1 min, capturing the average from the last 1, 5, and 15 minutes. Please use Pythonlibs and not system commands. Each metric sample should be inserted to the DB using Python influxdb lib (it is important to think about big-scale operations in your design)
- [x] Using Dockerfile create a container and make it execute your script. Update your docker-compose file to include the new container.
- [x] Make sure you can query Influx for the metrics.
- [x] BONUS: add Grafana container to the docker-compose and create a graph to show the load over time

TODO:
- [ ] Add datasources.yaml to be passed to grafana
- [ ] Pass load average dashboard to grafana provisioning

